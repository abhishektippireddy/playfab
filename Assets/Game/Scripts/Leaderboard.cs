﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Leaderboard : MonoBehaviour
{
    public Button postLeaderboardScoreButton;
    public Button getLeaderboardScoreButton;

    public Text postText;
    public Text getText;

    void Start()
    {
        postLeaderboardScoreButton.enabled = false;
        getLeaderboardScoreButton.enabled = false;

        postText.enabled = false;
        getText.enabled = false;
    }

    private void FixedUpdate()
    {
        if (PlayFabManager.Instance.state == PlayFabManager.LoginState.Success)
        {
            postLeaderboardScoreButton.enabled = true;
            getLeaderboardScoreButton.enabled = true;
            postText.enabled = true;
            getText.enabled = true;
        }
    }

    #region Playfab Local Updates
    public void onPostLeaderboardScore()
    {
        int score = -1;
        if (int.TryParse(postText.text, out score) == false)
        {
            Debug.Log("ERROR with score");
            return;
        }

        PlayFabClientAPI.UpdatePlayerStatistics(
            new UpdatePlayerStatisticsRequest
            {
                Statistics = new List<StatisticUpdate>
                {
                    new StatisticUpdate { StatisticName = "high_score", Value = score }
                }
            },
            new System.Action<UpdatePlayerStatisticsResult>(UpdatePlayerStatisticsResponse),
            new System.Action<PlayFabError>(LeaderboardErrorCallback)
        );
    }

    public void UpdatePlayerStatisticsResponse(UpdatePlayerStatisticsResult result)
    {
        Debug.Log("User statistics updated");
    }

    public void onGetLeaderboardScore()
    {
        PlayFabClientAPI.GetPlayerStatistics(
            new GetPlayerStatisticsRequest
            {
                StatisticNames = new List<string>
                {
                    "high_score"
                }
            },
            new System.Action<GetPlayerStatisticsResult>(PlayerStatisticResultCallback),
            new System.Action<PlayFabError>(LeaderboardErrorCallback)
        );

    }


    public void PlayerStatisticResultCallback(GetPlayerStatisticsResult result)
    {
        foreach (var statResult in result.Statistics)
        {
            if (statResult.StatisticName == "high_score")
            {
                getText.text = statResult.Value.ToString();
                break;
            }
        }
    }

    public void LeaderboardResultCallback(GetLeaderboardResult result)
    {
        string boardName = (result.Request as GetLeaderboardRequest).StatisticName;

        Debug.Log("Leaderboard results " + boardName);
        foreach (PlayerLeaderboardEntry entry in result.Leaderboard)
        {
            Debug.Log(string.Format("Player {0} has Rank {1} with a score of {2}", entry.DisplayName, entry.Position, entry.StatValue));
        }
    }

    public void LeaderboardErrorCallback(PlayFabError error)
    {
        Debug.LogError("Leaderboard had an error:");
        Debug.LogError(error.GenerateErrorReport());
    }
    #endregion
}
