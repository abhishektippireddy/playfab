﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogPokemon : MonoBehaviour
{
    //text fields for pokemon name lattitude and longitude
    public Text PokemonName;
    public Text Lattitude;
    public Text Longitude;

    //when catch button is pressed an API request to store the pokemon name and location
    public void onCatchPokemon()
    {
        if (PokemonName.text != "Name")
        {
            var request = new WriteClientPlayerEventRequest
            {
                Body = new Dictionary<string, object>()
            {
                { "Pokemon", PokemonName.text },
                { "Location", Lattitude.text +","+ Longitude.text }
            },
                EventName = "Pokemon_Caught"
            };
            PlayFabClientAPI.WritePlayerEvent(request, OnWriteSuccess, OnWriteFailure);
        }
    }
    //when the request returns suceess
    private void OnWriteSuccess(WriteEventResponse result)
    {
        Debug.Log("Write Data Success");
    }
    //on failure
    private void OnWriteFailure(PlayFabError error)
    {
        Debug.Log(error.ErrorMessage);
    }
}
