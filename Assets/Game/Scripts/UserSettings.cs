﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UserSettings : MonoBehaviour
{
    public Button updateUserNameButton;
    public Text userNameText;
    public Text userName;
    public bool getname = true;
    public Text PokemonCount;
    void Start()
    {
        updateUserNameButton.enabled = false;
        userNameText.enabled = false;
        PokemonCount.text = "0";
    }

    private void FixedUpdate()
    {
        if (PlayFabManager.Instance.state == PlayFabManager.LoginState.Success)
        {
            updateUserNameButton.enabled = true;
            userNameText.enabled = true;
            if (getname)
            {
                getUserName();
                getname = false;
                getPokemonCount();
            }
        }
    }

    //updates the user name when ever update button is clicked
    public void updateUserName()
    {
        PlayFabClientAPI.UpdateUserTitleDisplayName(
            new UpdateUserTitleDisplayNameRequest() { DisplayName = userNameText.text },
            result =>
            {
                userName.text = userNameText.text;
                Debug.Log("Success");
            },
            error =>
            {
                Debug.Log("Failed to update username");
            }
        );
    }

    //retreives the user name from the playfab data
    public void getUserName()
    {
        var request = new GetPlayerProfileRequest
        {
            PlayFabId = PlayFabManager.Instance.platFabID,
            ProfileConstraints = new PlayerProfileViewConstraints()
            {
                ShowDisplayName = true
            }
        };
        PlayFabClientAPI.GetPlayerProfile(request, OnUserNameSuccess, OnUserNameFailure);
    }
    void OnUserNameSuccess(GetPlayerProfileResult result)
    {
        string name = "";
        name+= result.PlayerProfile.DisplayName;
        if (name == "")
        {
            userName.text = "No Name";  //post "No Name" when a new user or a user with no name logs in
        }
        else
        {
            userName.text = result.PlayerProfile.DisplayName;
        }

    }

    private void OnUserNameFailure(PlayFabError error)
    {
        Debug.LogError("Something went wrong  " + error.ErrorMessage);
    }

    //to fetch the pokemon count from the playfab 
    public void getPokemonCount()
    {
        var request = new GetPlayerStatisticsRequest
        {
            StatisticNames = new List<string>
            {
                    "Pokemon_Count"
            }
        };
        
        PlayFabClientAPI.GetPlayerStatistics(request, OnCountSuccess, OnCountFailure);
    }
    //to store the pokemon count into the playfab
    public void postPokemonCount(int score)
    {
        var request = new UpdatePlayerStatisticsRequest
        {
            Statistics = new List<StatisticUpdate>
            {
                    new StatisticUpdate { StatisticName = "Pokemon_Count", Value = score }
            }
        };
        PlayFabClientAPI.UpdatePlayerStatistics(request, OnPostSuccess, OnCountFailure);
    }
    
    private void OnCountSuccess(GetPlayerStatisticsResult result)
    {
        foreach (var statResult in result.Statistics)
        {
            if (statResult.StatisticName == "Pokemon_Count")
            {

                    PokemonCount.text = statResult.Value.ToString();
                    break;
            }
        }
    }
    public void OnPostSuccess(UpdatePlayerStatisticsResult result)
    {
        getPokemonCount();
    }
    private void OnCountFailure(PlayFabError error)
    {
    }

    //update sthe pokemon count when a new pokemon is caught and posts it to playfab
    public void UpdatePokemonCount()
    {
        int count = int.Parse(PokemonCount.text);
        count++;
        postPokemonCount(count);
    }
}
