﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
//classes required to read the json from the url
[System.Serializable]
public class PokemonInfo
{
    public PokemonSprites sprites;
    public PokemonName species;
}
[System.Serializable]
public class PokemonSprites
{
    public string front_default;
}
[System.Serializable]
public class PokemonName
{
    public string name;
}
public class Pokemon : MonoBehaviour
{
    public Text pokemonName;
    public Image pokemonImage;
    public Text DisplayName;
    string PokemonURL = "https://pokeapi.co/api/v2/pokemon/"; //Url for searching a pokemon
    byte[] textureData;     //texture of the pokemon 

    //searching for a pokemon
    public void catchPokemon()
    {
        if(pokemonName.text!="")
        {
            PokemonInfo pokemonInfo = GetPokemonInfo(PokemonURL + pokemonName.text + "/");
            StartCoroutine( GetPokemonTexture(pokemonInfo));
            DisplayName.text = pokemonInfo.species.name;
        }
    }
    //fetches the json fron the Url
    public PokemonInfo GetPokemonInfo(string URL)
    {
        try
        {
            HttpWebRequest request = WebRequest.CreateHttp(URL);
            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string jsonResponse = reader.ReadToEnd();
            reader.Close();
            reader.Dispose();
            return JsonUtility.FromJson<PokemonInfo>(jsonResponse);
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
        }
        return null;
    }
    //gets  the texture of the pokemon and stores tit a byte array
    IEnumerator GetPokemonTexture(PokemonInfo pokemonInfo)
    {       
        UnityWebRequest webRequest = UnityWebRequest.Get(pokemonInfo.sprites.front_default);
        yield return webRequest.SendWebRequest();
        if (webRequest.isNetworkError || webRequest.isHttpError)
        {
            Debug.Log(webRequest.error);
        }
        else
        {
            textureData = webRequest.downloadHandler.data;
            CreatePokemonSprite(textureData);
        }
     
    }
    //creates a sprite from the byte array texture 
    public void CreatePokemonSprite(byte[] textureData)
    {
        Texture2D tex = new Texture2D(96, 96);
        tex.LoadImage(textureData);
        pokemonImage.sprite = Sprite.Create(tex, new Rect(0,0,96,96), new Vector2(0, 0),100.0f);
    }
}
