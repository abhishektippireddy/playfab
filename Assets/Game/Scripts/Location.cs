﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Location : Singleton<Location>
{
    public enum LocationServiceState
    {
        Waiting,
        Searching,
        Ready,
        Failed
    }
    public LocationServiceState state = LocationServiceState.Waiting;
    public float lattitude;
    public float longitude;

    public Text lattitudeText;
    public Text longitudeText;

    public float RefreshTime = 10.0f; //Location Refresh time
    float TimeElapsed = 0;             //counter for time elapsed
    // Start is called before the first frame update
    void Start()
    {
        lattitudeText.text = "Not available";
        longitudeText.text = "Not available";
        GetLocation();          //get location when the app starts
    }
    private void Update()
    {
        TimeElapsed += Time.deltaTime;  //time elapsed each frame
        if(TimeElapsed>=RefreshTime)
        {
            GetLocation();              //get the loaction after refreshtime
            TimeElapsed = 0;
        }
    }
    public void GetLocation()
    {
        if (state != LocationServiceState.Searching)
        {
            state = LocationServiceState.Searching;
            StartCoroutine(LocationServiceUpdate());
        }
    }
    IEnumerator LocationServiceUpdate()
    {
        Input.location.Start();
        float waitTime = 20.0f;
        while (Input.location.status == LocationServiceStatus.Initializing)
        {
            yield return new WaitForSeconds(1);
            waitTime--;
        }
        if (waitTime <= 0)
        {
            lattitudeText.text = "Not available";
            longitudeText.text = "Not available";
            state = LocationServiceState.Failed;
            yield break;
        }
        if (Input.location.status == LocationServiceStatus.Failed || Input.location.status == LocationServiceStatus.Stopped)
        {
            lattitudeText.text = "Not available,failed";
            longitudeText.text = "Not available,failed";
            state = LocationServiceState.Failed;
            yield break;
        }
        lattitude = Input.location.lastData.latitude;
        longitude = Input.location.lastData.longitude;

        lattitudeText.text = lattitude.ToString();
        longitudeText.text = longitude.ToString();

        Input.location.Stop();
        state = LocationServiceState.Ready;
    }
}
